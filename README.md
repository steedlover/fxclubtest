## Install steps ##

#### 1) Cloning ####
```
#!bash

git clone https://steedlover@bitbucket.org/steedlover/fxclubtest.git ./proj
cd ./proj
```
#### 2) Installing all the necessary dependencies ####
```
#!bash
npm install
```
#### 3) Building the project ####
```
#!bash
gulp build
```
#### 4) Starting server ####
```
#!bash
npm start

Project will be available on http://localhost:8080  (if this port is free for reserving)
```
