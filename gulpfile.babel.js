'use strict';

// include the required packages.
const gulp = require('gulp');
const changed = require('gulp-changed');
const $ = require('gulp-load-plugins')();
const stripCssComments = require('gulp-strip-css-comments');

const buildPath = 'build/';

// include, if you want to work with sourcemaps
// var sourcemaps = require('gulp-sourcemaps');

/* Fonts */
gulp.task('fonts', function () {
  gulp.src('app/styles/fonts/*')
    .pipe(gulp.dest(buildPath + 'css/fonts'));
});

/* Flags */
gulp.task('flags', function () {
  gulp.src('node_modules/flag-icon-css/flags/4x3/*.svg')
    .pipe(gulp.dest(buildPath + 'flags/4x3'));
});

/* Static js files */
gulp.task('staticjs', function () {
  gulp.src('app/static/js/*.js')
    .pipe(gulp.dest(buildPath + 'js'));
});

/* minify images */
gulp.task('images', function () {
  gulp.src('app/img/**/*')
    .pipe(changed(buildPath + 'img'))
    .pipe($.imagemin({
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest(buildPath + 'img'));
});

/* minify the html */
gulp.task('minify-html', function () {
  return gulp.src('app/**/*.html',
    {
      base: 'app/'
    })
    .pipe(changed(buildPath))
    .pipe($.htmlmin({
      collapseWhitespace: true,
      removeComments: true,
      removeTagWhitespace: true,
      minifyJS: true
    }))
    .pipe(gulp.dest(buildPath));
});

gulp.task('basejs', function() {
  gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/bootstrap/dist/js/bootstrap.js'
  ])
    .pipe($.uglifyjs())
    .pipe($.concat('base.js'))
    .pipe(gulp.dest(buildPath + '/js'));
});

gulp.task('js', function() {
  gulp.src('app/js/**/*.js')
    .pipe($.babel({
      presets: ['es2015', 'stage-2']
    }))
    .pipe($.concat('scripts.js'))
    .pipe(gulp.dest(buildPath + '/js'));
});

// Get one .styl file and render
gulp.task('styles', function () {
  return gulp.src('app/styles/styles.styl')
    .pipe($.stylus({
      compress: true,
      'include css': true,
    }))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe(stripCssComments({
      preserve: false
    }))
    .pipe($.csso())
    .pipe(gulp.dest(buildPath + '/css'));
});

// Default gulp task to run
gulp.task('default', ['minify-html', 'basejs', 'fonts', 'js', 'styles', 'flags', 'images', 'staticjs', 'watch']);
gulp.task('build', ['minify-html', 'basejs', 'fonts', 'js', 'styles', 'flags', 'images', 'staticjs']);
gulp.task('watch', function () {
  $.watch('app/**/*.html', () => gulp.start('minify-html'));
  $.watch('app/styles/**/*.styl', () => gulp.start('styles'));
  $.watch('app/js/**/*.js', () => gulp.start('js'));
});


