/* global $ */
// to prevent displaying the errors by JShint plugin I should point the global variables

'use strict';

$(document).ready(() => {

  const phoneField = $('#formPhone');
  const formButton = $('#formButton');
  const formName = $('#formName');
  const formSurname = $('#formSurname');
  const formEmail = $('#formEmail');
  const phonePattern = /^[0-9-+]+$/;
  const stringPattern = /^[^0-9]+$/;
  const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const errClass = 'error';

  disableFormButton(); // Disable button on start
  let formReady = {
    phone: false,
    name: false,
    surname: false,
    email: false
  };

  function phoneCheck(el) {
    const val = $(el).val();
    if (val.length !== 16 || !phonePattern.test(val)) {
      $(el).addClass(errClass);
      return false;
    } else if ($(el).hasClass(errClass)) {
      $(el).removeClass(errClass);
    }
    return true;
  }

  function stringCheck(el) {
    const val = $(el).val();
    if (val.length < 2 || !stringPattern.test(val)) {
      $(el).addClass(errClass);
      return false;
    } else if ($(el).hasClass(errClass)) {
      $(el).removeClass(errClass);
    }
    return true;
  }

  function emailCheck(el) {
    const val = $(el).val();
    if (val.length < 4 || !emailPattern.test(val)) {
      $(el).addClass(errClass);
      return false;
    } else if ($(el).hasClass(errClass)) {
      $(el).removeClass(errClass);
    }
    return true;
  }

  phoneField.blur((e) => {
    if (!phoneCheck(e.currentTarget)) {
      disableFormButton();
      formReady.phone = false;
    } else {
      formReady.phone = true;
    }
  });

  formName.blur((e) => {
    if (!stringCheck(e.currentTarget)) {
      disableFormButton();
      formReady.name = false;
    } else {
      formReady.name = true;
      if (formCheck()) {
        enableFormButton();
      }
    }
  });

  formSurname.blur((e) => {
    if (!stringCheck(e.currentTarget)) {
      disableFormButton();
      formReady.surname = false;
    } else {
      formReady.surname = true;
      if (formCheck()) {
        enableFormButton();
      }
    }
  });

  formEmail.blur((e) => {
    if (!emailCheck(e.currentTarget)) {
      disableFormButton();
      formReady.email = false;
    } else {
      formReady.email = true;
      if (formCheck()) {
        enableFormButton();
      }
    }
  });

  function disableFormButton() {
    formButton.attr('disabled', true);
  }

  function enableFormButton() {
    formButton.removeAttr('disabled');
  }

  function formCheck() {
    let result = true;
    $.each(formReady, (key, val) => {
      if (val === false) {
        result = false;
      }
    });
    return result;
  }

  formButton.click(() => {
    if (!formCheck()) {
      disableFormButton();
    } else {
      setTimeout(() => {
        formName.attr('readonly', true);
        phoneField.attr('readonly', true);
        formSurname.attr('readonly', true);
        formEmail.attr('readonly', true);
        disableFormButton();
        $('.form-message').removeClass('hidden');
      }, 2000);
    }
  });

});
