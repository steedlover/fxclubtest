/* global $ */
// to prevent displaying the errors by JShint plugin I should point the global variables

'use strict';

const specButtonsCodes = [8, 9, 116, 37, 39];

function preventNumberInput(e) {
  const keyCode = (e.keyCode ? e.keyCode : e.which);
  // It's allowed to input numbers and push some specials buttons such as F5, Backspace
  if (keyCode > 47 && keyCode < 58 || specButtonsCodes.includes(keyCode)) {
    return true;
  } else {
    e.preventDefault();
    return false;
  }
}

$(document).ready(() => {

  const phoneField = $('#formPhone');

  phoneField.on('change paste keyup', (e) => {
    let elVal = e.target.value;
    const backSpaced = e.originalEvent && typeof e.originalEvent.key === 'string' && e.originalEvent.key.toLowerCase() === 'backspace' ? true : false; // Check if Backspace has been pushed
    const startLength = elVal.length;
    if (elVal.length > 0) {
      const newSymbol = elVal.slice(-1);
      if (!backSpaced && startLength === 1) {
        elVal = '+' + newSymbol;
        phoneField.val(elVal);
      }
      if ((elVal.length === 4 || elVal.length === 8 || elVal.length === 12) && !backSpaced) {
        elVal += '-';
        phoneField.val(elVal);
      }
      if (backSpaced && (elVal.length === 4 || elVal.length === 8 || elVal.length === 12)) {
        elVal = elVal.slice(0, -1);
        phoneField.val(elVal);
      }
      if (backSpaced && elVal.length === 1) {
        elVal = '';
        phoneField.val(elVal);
      }
    }
  });

});
