/* global $ */

'use strict';

const currency = '$';
const fields = ['id', 'value', 'account', 'country'];
const ascSortClass = 'fa-sort-down';
const descSortClass = 'fa-sort-up';
let scores = [
  {
    id: 1,
    value: 3200,
    account: '439 578 241',
    country: 'us'
  },
  {
    id: 2,
    value: 6400,
    account: '123 456 789 012',
    country: 'gb'
  },
  {
    id: 3,
    value: 550,
    account: '129 1289 45',
    country: 'ch'
  },
  {
    id: 4,
    value: 1235,
    account: '1212 4576 34',
    country: 'se'
  },
  {
    id: 5,
    value: 10540,
    account: '794356',
    country: 'ru'
  },
  {
    id: 6,
    value: 120,
    account: '1238 3',
    country: 'ru'
  },
  {
    id: 7,
    value: 1000,
    account: '777',
    country: 'pr'
  },
  {
    id: 8,
    value: 25,
    account: '111 5407 12',
    country: 'gb'
  },
  {
    id: 9,
    value: 3000,
    account: '1',
    country: 'um'
  },
  {
    id: 10,
    value: 12500,
    account: '100 550',
    country: 'um'
  }
];

let localSortVal = localStorage.getItem('sortVal');
// If localStorage val doesn't exist so take the first fields element
let sortVal = typeof localSortVal === 'string' ? localSortVal : fields[0];
// The same behaviour for localStorage ASC value, 'false' if it doesn't esist
let localSortAsc = localStorage.getItem('sortAsc');
// Since the localStorage contents all the values as strings we have to compare them with string
let sortAsc = typeof localSortAsc === 'string' && localSortAsc === 'true' ? true : false;


// Function for the money prop. Adds spaces between thousands
function format(num) {
  const n = String(num), p = n.indexOf(' ');
  return n.replace(
    /\d(?=(?:\d{3})+(?:\.|$))/g,
    (m, i) => p < 0 || i < p ? `${m} ` : m
  );
}

$(document).ready(() => {

  const tableWrapper = $('.table-body');
  scores = sortBy(sortVal, sortAsc);
  rebuildList(scores);

  $('.table-header-row .fa').click((e) => {
    let asc;
    let el = $(e.currentTarget);
    if (el.hasClass(ascSortClass)) {
      asc = true;
    } else if (el.hasClass(descSortClass)) {
      asc = false;
    }
    let parentClasses = el.parent().parent().attr('class');
    let sortClass;
    let parentClassArr = parentClasses.split('-');
    if (parentClassArr.length > 1) {
      sortClass = parentClassArr[1];
    }
    if (sortClass === sortVal) {
      asc = !sortAsc;
    }
    if (sortClass && fields.includes(sortClass)) {
      let newList = sortBy(sortClass, asc);
      rebuildList(newList);
    }
  });

  function rebuildList(list) {
    tableWrapper.empty();

    $.each(list, (key, el) => {
      let row = $('<div/>').addClass('table-body-row');
      $('<div/>').text(el.id).appendTo(row);
      $('<div/>').appendTo(row);
      $('<div/>').text(currency + format(el.value)).appendTo(row);
      $('<div/>').text(el.account).appendTo(row);
      let countryBlock = $('<div/>');
      $('<i/>').addClass('flag-icon flag-icon-' + el.country).appendTo(countryBlock);
      countryBlock.appendTo(row);
      row.appendTo(tableWrapper);
    });
  }

  function sortBy(val, asc) {
    asc = typeof asc === 'boolean' ? asc : true;
    let result = scores;
    if (fields.includes(val)) {
      result = scores.sort((a, b) => {
        if (asc) {
          return a[val] < b[val];
        } else {
          return a[val] > b[val];
        }
      });
      sortAsc = asc;
      localStorage.setItem('sortAsc', asc); // Write the value to localStorage
      sortVal = val;
      localStorage.setItem('sortVal', val); // Write the value to localStorage
      setSortingLabel(val, asc);
    }
    return result;
  }

  function setSortingLabel(val, asc) {
    let sortCol = $('.sort-' + val);
    if (sortCol) {
      let direction = !asc ? 'up' : 'down';
      $('.fa').removeClass('active'); // Remove all the existing classes
      let label = sortCol.children('.sorting').children('.fa-sort-' + direction);
      label.addClass('active');
    }
  }

});
