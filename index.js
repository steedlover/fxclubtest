// setting up the server

'use strict';
const http = require('http');
const express = require('express');
const app = express();

//-- vars
const port = process.env.PORT || 8080;
let server = http.createServer(app);
process.env.mode = process.env.mode || 'local';

//--- serving static files
app.use(express.static(__dirname + '/build'));

console.log(process.env.mode);

//--- start the http server
server.listen(port);
console.log('server started at port: ' + port + ' on ' + process.env.mode);
